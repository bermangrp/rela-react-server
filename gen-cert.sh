docker run \
	-p 80:80 \
	-p 443:443 \
	-it \
	--rm \
	-v "$(pwd)/out":/acme.sh  \
	neilpang/acme.sh --issue -d TEMPORARY.HOST.COM --standalone --keylength ec-256;
cp $(pwd)/out/TEMPORARY.HOST.COM_ecc/fullchain.cer /root/wp-docker-react/secrets/localhost.crt;
cp $(pwd)/out/TEMPORARY.HOST.COM_ecc/TEMPORARY.HOST.COM.key /root/wp-docker-react/secrets/localhost.key;