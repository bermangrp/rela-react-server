# System Setup
- install docker
- install yarn

# Development

## Start Docker
    docker-compose -f docker-compose.yml up --build -d

## Install Wordpress
- go to http://localhost:3000
- activate the model builder plugin

## Build node app
	cd volumes/node_app
	yarn install
	yarn run dev

# Production

## Local Setup
----
#### Copy this readme, find and replace the following terms and then [use a markdown editor](http://markdownlivepreview.com/) to complete the rest of the document
	YOUR_GITLAB_TOKEN
	YOUR_DIGITAL_OCEAN_KEY
	FINAL-HOST.COM
	TEMPORARY.HOST.COM
	NEW_USERNAME
	USER@GITLAB.COM
	SERVERNAME
	NEW\_THEME\_NAME
	YOUR_MACHINE
	YOUR_URL
	YOUR\_URL
	YOUR_USERNAME
	YOUR_PASSWORD

#### Setup variables (local)

	echo export GITLAB_TOKEN=YOUR_GITLAB_TOKEN >> ~/.bash_profile
	echo export DigO_TOKEN=YOUR_DIGITAL_OCEAN_KEY >> ~/.bash_profile
	source ~/.bash_profile

#### Create an ssh key for your device and then log in and add to [GitLab](http://www.gitlab.com)
	ssh-keygen -t rsa -C "USER@GITLAB.COM"
	cat ~/.ssh/id_rsa.pub
	curl -X POST "https://gitlab.com/api/v4/user/keys" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" -H "Content-Type: application/json" -d'{"title":"YOUR_MACHINE-computer-key","key":"THE_KEY_THAT_YOU_JUST_MADE"}'

## Server Setup
----
### Create a new server (local)
    curl -X POST "https://api.digitalocean.com/v2/droplets" -H "Authorization: Bearer $DigO_TOKEN" -H "Content-Type: application/json" -d'{"name":"FINAL-HOST.COM","region":"nyc3","size":"1gb","image":"docker-18-04","backups":"true"}'

### Update A records (dns provider)
Use bermangrp.com DNS if none available

## Repo Setup
----
### Create keys
>#### Generate key  (server)
	ssh-keygen -t rsa -C "USER@GITLAB.COM"
	cat ~/.ssh/id_rsa.pub
#### Add key to Gitlab (local)
	curl -X POST "https://gitlab.com/api/v4/user/keys" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" -H "Content-Type: application/json" -d'{"title":"SERVERNAME-server-key","key":"THE_KEY_THAT_YOU_JUST_MADE"}'
#### Clone repo (server)
	git clone git@gitlab.com:bermangrp/wp-docker-react.git
#### Check DNS (local)
	ping TEMPORARY.HOST.COM
#### Get certificate
	docker run \
		-p 80:80 \
		-p 443:443 \
		-it \
		--rm \
		-v "$(pwd)/out":/acme.sh  \
		neilpang/acme.sh --issue -d TEMPORARY.HOST.COM --standalone --keylength ec-256;
	cp $(pwd)/out/TEMPORARY.HOST.COM_ecc/fullchain.cer /root/wp-docker-react/secrets/localhost.crt;
	cp $(pwd)/out/TEMPORARY.HOST.COM_ecc/TEMPORARY.HOST.COM.key /root/wp-docker-react/secrets/localhost.key;
#### Start server
	cd /root/wp-docker-react;
	sh build.sh;