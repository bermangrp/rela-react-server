#### Build the image
	docker build -t wfaye/node-web-app .

#### Run the image
	docker run -p 49160:8080 -d wfaye/node-web-app

#### Log into the container
	docker exec -it 4c8b99be5c64 /bin/bash

#### List all containers
	docker ps -a

#### Stop all containers
	docker stop $(docker ps -a -q)

#### Remove all containers
	docker rm -f $(docker ps -a -q)

#### Remove all images
    docker rmi -f $(docker images -a -q)

#### Remove all volumes
    docker volume rm $(docker volume ls -f dangling=true -q)

#### Log into container
    docker exec -it 207e02dfb234 bash

#### Run using compose
    docker-compose up
    docker-compose -f docker-compose.yml up -d
    docker-compose -f docker-compose.yml up --build -d

#### Run commands as web
    su - web -c "COMMAND"

#### Purging All Unused or Dangling Images, Containers, Volumes, and Networks (and additionally remove any stopped containers and all unused images)
    docker system prune -a

#### Reset
    docker stop $(docker ps -aq);
    docker rm $(docker ps -aq);
    rm -rf volumes/*/data;
    docker volume rm $(docker volume ls -f dangling=true -q);
    mkdir volumes/db/data/;
    mkdir volumes/mb-plugin/data/;
    mkdir volumes/node_app/data/;
    mkdir volumes/uploads/data/;
    docker-compose -f docker-compose.yml up --build -d;
