const express = require('express');
const https = require("https");
const fs = require("fs");
const { execSync } = require('child_process');
const app = express();

const options = {
  key: fs.readFileSync("/etc/nginx/localhost.key"),
  cert: fs.readFileSync("/etc/nginx/localhost.crt")
};

app.all('/', function (req, res) {
	var result = execSync('npm run pull');
	res.send(result.toString('utf8'));
});

https.createServer(options, app).listen(8181);