cd /usr/share/nginx/repos && \
find . \
	-type d \
	-mindepth 1 \
	-maxdepth 1 \
	-exec git --git-dir={}/.git --work-tree=$PWD/{} pull origin master --allow-unrelated-histories \;