docker stop $(docker ps -aq);
docker rm $(docker ps -aq);
rm -rf volumes/*/data;
docker volume rm $(docker volume ls -f dangling=true -q);
