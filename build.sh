mkdir -p volumes/db/data/;
mkdir -p volumes/mb-plugin/data/;
mkdir -p volumes/node_app/data/;
mkdir -p volumes/uploads/data/;
(cd secrets && sh gen-ssl.sh);
docker-compose -f docker-compose.yml up --build -d;